package com.xpeppers.contacts.integration.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.UUID;

import org.junit.Test;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;

import com.xpeppers.contacts.client.ContactSvcApi;
import com.xpeppers.contacts.contact.TestData;
import com.xpeppers.contacts.repository.Contact;

public class ContactSvcClientApiTest {

	private final String TEST_URL = "http://localhost:8080";

	private ContactSvcApi service = new RestAdapter.Builder()
			.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL).build()
			.create(ContactSvcApi.class);

	private Contact contact = TestData.randomContact();

	@Test
	public void testContactAddAndList() throws Exception {
		contact = service.addContact(contact);
		assertTrue(contact != null);

		Collection<Contact> contacts = service.getContactList();
		assertTrue(contacts.contains(contact));
	}

	@Test
	public void testContactAddAndEdit() throws Exception {
		Contact c = contact;
		c = service.addContact(c);
		assertTrue(c != null);

		Collection<Contact> contacts = service.getContactList();
		assertTrue(contacts.contains(c));

		c.setName(UUID.randomUUID().toString());
		c.setSurname(UUID.randomUUID().toString());
		c.setTelephone(TestData.randomTelephone());

		c = service.editContact(c);
		assertTrue(c != null);

		contacts = service.getContactList();
		assertTrue(contacts.contains(c));
	}

	@Test
	public void testContactRejectAdd() throws Exception {
		for (int i = 0; i < 9; i++) {
			Contact c = contact;
			switch (i) {
			case 0:
				c.setName(null);
				break;
			case 1:
				c.setSurname(null);
				break;
			case 2:
				c.setName("");
				break;
			case 3:
				c.setSurname("");
				break;
			case 4:
				c.setTelephone("39 02 1234567");
				break;
			case 5:
				c.setTelephone("+39 021234567");
				break;
			case 6:
				c.setTelephone("+3902 1234567");
				break;
			case 7:
				c.setTelephone("+39 02 12345");
				break;
			case 8:
				c.setTelephone("+39 02 12a34567");
				break;
			}
			c = service.addContact(c);
			assertFalse(c != null);

			Collection<Contact> contacts = service.getContactList();
			assertFalse(contacts.contains(c));
		}
	}

	@Test
	public void testContactFind() throws Exception {
		contact = service.addContact(contact);
		assertTrue(contact != null);

		Collection<Contact> contacts;

		contacts = service.findByNameOrSurnameOrTelephone(contact.getName());
		assertTrue(contacts.contains(contact));

		contacts = service.findByNameOrSurnameOrTelephone(contact.getSurname());
		assertTrue(contacts.contains(contact));

		contacts = service.findByNameOrSurnameOrTelephone(contact
				.getTelephone());
		assertTrue(contacts.contains(contact));

		Contact contactById = service.findById(contact.getId());
		assertTrue(contact.equals(contactById));
	}
}
