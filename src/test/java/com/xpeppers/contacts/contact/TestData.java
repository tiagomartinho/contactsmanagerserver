package com.xpeppers.contacts.contact;

import java.util.Random;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xpeppers.contacts.repository.Contact;

public class TestData {

	private static final ObjectMapper objectMapper = new ObjectMapper();

	public static Contact randomContact() {
		String id = UUID.randomUUID().toString();
		String name = id;
		String surname = id;
		String telephone = randomTelephone();
		return new Contact(name, surname, telephone);
	}

	public static String randomTelephone() {
		return "+" + randomNumbers(randInt(1,3)) + " " + randomNumbers(randInt(1,3)) + " " + randomNumbers(randInt(6,10));
	}

	static Random rand = new Random();

	public static int randInt(int min, int max) {
	    return rand.nextInt((max - min) + 1) + min;
	}
	
	private static String randomNumbers(int n) {
		String result = "";
		for (int i = 0; i < n; i++)
			result += String.valueOf(rand.nextInt(10));
		return result;
	}

	public static String toJson(Object o) throws Exception {
		return objectMapper.writeValueAsString(o);
	}
}
