package com.xpeppers.contacts.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.xpeppers.contacts.client.ContactSvcApi;
import com.xpeppers.contacts.repository.Contact;
import com.xpeppers.contacts.repository.ContactRepository;

@Controller
public class ContactSvc implements ContactSvcApi {

	@Autowired
	private ContactRepository contacts;

	@RequestMapping(value = ContactSvcApi.CONTACT_SVC_PATH, method = RequestMethod.POST)
	public @ResponseBody Contact addContact(@RequestBody Contact c) {

		if(!isContactValid(c))
			return null;

		//Adds the contact to the repository, returns the contact added
		return contacts.save(c);
	}

	private boolean isContactValid(Contact c)
	{
		// Verifies that neither of the fields are null or empty
		if (c.getName() == null || c.getName().isEmpty()
				|| c.getSurname() == null || c.getSurname().isEmpty()
				|| c.getTelephone() == null || c.getTelephone().isEmpty())
			return false;

		// Verifies that the telephone is in the specified format
		// +39 02 1234567
		// That is a "+" followed by a nonempty group of digits, a space, a
		// nonempty group of digits, a space, a group of digits with at least 6
		// digits

		// First is verified that it has the format XXX YYY ZZZZZZ
		String[] telephone = c.getTelephone().split(" ");
		if (telephone.length != 3 || telephone[0].length() < 2
				|| telephone[1].length() < 1 || telephone[2].length() < 6)
			return false;

		// Second is verified that it starts with a '+' sign and that all the
		// remaining are numbers
		for (int i = 0; i < telephone.length; i++)
			for (int j = 0; j < telephone[i].length(); j++) {
				if (i == 0 && j == 0) {
					if (telephone[i].charAt(j) != '+')
						return false;
				} else {
					if (telephone[i].charAt(j) < '0'
							|| telephone[i].charAt(j) > '9')
						return false;
				}
			}
		
		return true;
	}
	
	@RequestMapping(value = ContactSvcApi.CONTACT_EDIT_PATH, method = RequestMethod.POST)
	public @ResponseBody Contact editContact(@RequestBody Contact c) {
		if(!isContactValid(c))
			return null;
		
		//If it finds the contact to edit, then deletes it
		Contact contact2edit = findById(c.getId());
		if (contact2edit == null)
			return null;
		contacts.delete(contact2edit);

		//Adds the contact edited and returns it
		return addContact(c);
	}

	@RequestMapping(value = ContactSvcApi.CONTACT_SVC_PATH, method = RequestMethod.GET)
	public @ResponseBody Collection<Contact> getContactList() {
		return Lists.newArrayList(contacts.findAll());
	}

	@RequestMapping(value = ContactSvcApi.CONTACT_SEARCH_PATH, method = RequestMethod.GET)
	public @ResponseBody Collection<Contact> findByNameOrSurnameOrTelephone(
			@RequestParam(QUERY_PARAMETER) String parameter) {
		return contacts.findBySurnameContainingIgnoreCaseOrNameContainingIgnoreCaseOrTelephoneContainingIgnoreCase(parameter,parameter,parameter);
	}

	@RequestMapping(value = ContactSvcApi.CONTACT_ID_SEARCH_PATH, method = RequestMethod.GET)
	public @ResponseBody Contact findById(@RequestParam(ID_PARAMETER) Long id) {
		if (id == null)
			return null;
		return contacts.findOne(id);
	}
}
