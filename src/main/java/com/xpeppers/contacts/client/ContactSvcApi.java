package com.xpeppers.contacts.client;

import java.util.Collection;

import com.xpeppers.contacts.repository.Contact;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public interface ContactSvcApi {
	
	public static final String QUERY_PARAMETER = "parameter";
	public static final String ID_PARAMETER = "id";

	public static final String CONTACT_SVC_PATH = "/contact";
	public static final String CONTACT_EDIT_PATH = CONTACT_SVC_PATH + "/edit";
	public static final String CONTACT_SEARCH_PATH = CONTACT_SVC_PATH + "/findByNameOrSurnameOrTelephone";
	public static final String CONTACT_ID_SEARCH_PATH = CONTACT_SVC_PATH + "/findById";

	@POST(CONTACT_SVC_PATH)
	public Contact addContact(@Body Contact c);
	
	@POST(CONTACT_EDIT_PATH)
	public Contact editContact(@Body Contact c);
	
	@GET(CONTACT_SVC_PATH)
	public Collection<Contact> getContactList();
		
	@GET(CONTACT_SEARCH_PATH)
	public Collection<Contact> findByNameOrSurnameOrTelephone(@Query(QUERY_PARAMETER) String parameter);
	
	@GET(CONTACT_ID_SEARCH_PATH)
	public Contact findById(@Query(ID_PARAMETER) Long id);
}
