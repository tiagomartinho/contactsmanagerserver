package com.xpeppers.contacts.repository;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.common.base.Objects;

@Entity
public class Contact {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String name;
	private String surname;
	private String telephone;

	public Contact() {
	}

	public Contact(String name, String surname, String telephone) {
		super();
		this.name = name;
		this.surname = surname;
		this.telephone = telephone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(name, surname, telephone);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Contact) {
			Contact other = (Contact) obj;
			return Objects.equal(name, other.name)
					&& Objects.equal(surname, other.surname)
					&& Objects.equal(telephone, other.telephone);
		} else {
			return false;
		}
	}
	
	public String toString() { 
	    return "[Id: " + this.id + " , Name: " + this.name + " , Surname: " + this.surname + " , Telephone: " + this.telephone +"]";
	} 
}
