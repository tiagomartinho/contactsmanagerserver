package com.xpeppers.contacts.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Long>{
	public Collection<Contact> findBySurnameContainingIgnoreCaseOrNameContainingIgnoreCaseOrTelephoneContainingIgnoreCase(String name,String surname, String telephone);
}
