Spring Server:

SW Needed:
Eclipse (Standard Version 4.4) - http://www.eclipse.org/downloads
+ Gradle IDE, In Eclipse select “Help” -> “Install New Software” 
http://dist.springsource.com/release/TOOLS/gradle

Import Gradle Project -> Select Project Folder
Build Model -> Finish

To Run:
Right Click in: src/main/java/com.xpeppers.contacts/Application.java
Select: Run as... -> Java Application

To Run Tests (And add some contacts to the repository):
Right Click in: src/test/java/com.xpeppers.contacts.integration.test/ContactSvcClientApiTest.java
Select: Run as... -> JUnit Test

http://localhost:8080/contact
